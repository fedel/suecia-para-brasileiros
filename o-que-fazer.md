# O que fazer quando se mudar para a Suécia?

Um pequeno guia com coisas importantes para se fazer antes e depois de se 
mudar para a Suécia. Esse guia é dividido por "momentos" do processo de
se mudar para a Suécia, em especial a região de Lund e Malmö no Skane.

## Antes de se mudar

### International CitizenHub
* https://internationalcitizenhub.com/
* Se cadastre na newsletter para receber notícias sobre palestras e cursos gratuitos
* Eles tem um programa para inserção no mercado de trabalho para conjuges (Kick-Start Program)

### Acompanhe notícias em Inglês no The Local
* https://www.thelocal.se/

## Quando chegar na Suécia

### Registrar-se no Skateverket
* https://www.skatteverket.se/
* Necessário para ter o personnummer

### Instalar o aplicativo Skanetrafiken no celular 
* Aplicativo para pagar os transportes (ônibus, trem, tram)
* https://play.google.com/store/apps/details?id=se.skanetrafiken.washington&hl=en_US&gl=US
* https://apps.apple.com/se/app/sk%C3%A5netrafiken/id1180539331?l=en
* É possível usar sem personnummer

### Comprar uma bicicleta
* Buscar em lojas de Segunda mão 
* Blocket (https://www.blocket.se/) - Site que pode ser usado para buscar moradia também
* Bicicletas de Leilão (em Sueco, mas é possível acompanhar) - https://lkpab.se/cyklar/cykelauktion/ (Lund) 
https://malmo.se/Service/Var-stad-och-var-omgivning/Trafik/For-dig-som-cyklar/Cykelauktioner.html (Malmö)

### Abrir conta no banco
* Normalmente precisa de personnummer, mas existem bancos como o SEB que 
permite abrir só com contrato de trabalho
* Se conseguir abrir conta sem o personnummer não consegue usar o BankID
* Fundamental para usar BankId e Swish

## Quando tiver seu personnummer

### Se cadastrar no Forsakringskassan
* https://www.forsakringskassan.se
* Necessário para ter acesso a certos benefícios como "Cartão de Saúde Europeu",
auxílio para dentista, auxílio doença e outros
* Mais fácil de cadastrar se tiver BankID

### Se cadastrar nas filas de imobiliárias
* LKF (https://www.lkf.se/) - Só lund
* Heimstaden (https://heimstaden.com/)
* Riksbyggen - https://www.riksbyggen.se/
* Boplats Syd (https://www.boplatssyd.se/) - Pago, assinatura anual

### BankId
* Permite acessar diversos serviços com a mesma identidade
* Necessário conta no banco
* Pedir para cadastrar quando abrir conta no banco para não precisar ir novamente

### Instalar Swish
* Aplicativo de transferência de dinheiro
* Precisa ter conta no banco

### Se inscrever no SFI (Sueco para Imigrantes)
* É necessário se inscrever na cidade que mora
* Lund : https://www.lund.se/en/education/adult-education/sfi---swedish-for-immigrants/
* Malmö: https://malmo.se/sfi

## Telefones úteis

* 112: Emergência (https://www.sosalarm.se/spraklanguages/english/)
* 1177 : Atendimento de saúde

## Sites úteis

### Kommuns
* Sites das Kommuns tem diversas informações sobre alertas, infomações, lazer,
inscrição em escolas...
* Lunds Kommun: https://lund.se/#/
* Malmö Stad: https://malmo.se/

### Estudos
* https://www.antagning.se/se/start

### Busca por empregos

* Serviço público de empregos - https://arbetsformedlingen.se/
* Guia para estudos e carreira (Lund) - https://www.lund.se/utbildning--forskola/vagledningscentrum-studie--och-yrkesvagledning/

## Outras dicas

### Biltema
* https://www.biltema.se/
* Ferramentas e itens para concerto de bicicleta, carro, casa...
* Vale uma visita para conhecer

### Transporte - Empréstimo
O aplicativo do skanetrafiken tem passes mensais que podem ser emprestados
para outro usuário do aplicativo. O empréstimo tem o prazo mínimo de 4 horas
e só pode ser emprestado 1 vez ao dia.
Com esse empréstimo é possível por exemplo que uma pessoa vá para o trabalho
com o passe, empreste o passe para outra pessoa que poderá usar durante o dia,
e no final do dia a primeira pessoa pode receber o passe de volta para voltar
do trabalho.

### Passe de verão
* No verão o Skanetrafiken disponibiliza um passe de verão que dura por volta
de 3 meses. Com esse passe você pode viajar quantas vezes quiser por todo o 
Skane
